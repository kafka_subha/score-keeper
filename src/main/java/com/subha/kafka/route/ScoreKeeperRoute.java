package com.subha.kafka.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * Created by subha on 09/07/2018.
 */

@Component
public class ScoreKeeperRoute extends RouteBuilder{

    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel("log:resultAnalyzer?level=ERROR&showHeaders=true&showBody=true&showCaughtException=true&showStackTrace=true")
                .useOriginalMessage());

        from("kafka:{{kafka.result-topic}}?brokers={{kafka.server}}:{{kafka.port}}&groupId={{kafka.channel}}&autoOffsetReset=earliest&consumersCount=1")
                .routeId("collect-score")
                .log("${body}");
    }
}
